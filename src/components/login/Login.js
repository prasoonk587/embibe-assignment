import React, { useState } from "react";
import InputField from "../common/inputField";
import logo from "../../assets/embibefullLogo.svg";

function Login() {
	const [userName, setUserName] = useState({
		value: "",
		error: "",
	});

	const [password, setPassword] = useState({
		value: "",
		error: "",
	});

	const validateForm = () => {
		if (
			userName.value &&
			!userName.error &&
			password.value &&
			!password.error
		) {
			return true;
		} else {
			setUserName({ ...userName, error: "This Field is required" });
			setPassword({ ...password, error: "This Field is required" });
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (validateForm()) {
			try {
				document.cookie = `userName=${userName.value}`;
				location.reload();
			} catch (err) {
				alert("Somenting went wrong");
			}
		}
	};

	return (
		<div className="container-fluid login-wrap">
			<div>
				<div className="logo-wrap">
					<img src={logo} />
				</div>
				<h4>Login</h4>
				<form onSubmit={handleSubmit}>
					<InputField
						value={userName.value}
						error={userName.error}
						setInputValue={setUserName}
						name="userName"
						placeholder="Enter User Name"
					/>
					<InputField
						name="password"
						value={password.value}
						error={password.error}
						type="password"
						setInputValue={setPassword}
						placeholder="Enter your Password"
					/>
					<div>
						<button>Login</button>
					</div>
				</form>
			</div>
		</div>
	);
}

export default Login;

import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { loadMarks } from "../../redux/actions/marksActions";
import Header from "../common/Header";

function MarksDetail({ marks, loadMarks, ...props }) {
	const [mark, setMark] = useState({ ...props.mark });

	useEffect(() => {
		if (marks.length === 0) {
			loadMarks().catch((err) => console.log(err));
		} else {
			setMark({ ...props.mark });
		}
	}, [props.mark]);
	return (
		<>
			<Header showFilters={false} />
			<div className="marks-detail-wrap">
				<div className="col-md-12 d-flex justify-content-center">
					<div className="col-md-6 col-sm-12">
						<table className="table table-striped">
							<tbody>
								<tr>
									<td>
										<b>Name</b>
									</td>
									<td>{mark.name}</td>
								</tr>
								<tr>
									<td>
										<b>Student ID</b>
									</td>
									<td>{mark.student_id}</td>
								</tr>
							</tbody>
						</table>
						<table className="table table-striped">
							<tbody>
								<tr className="table-info">
									<td>Subject</td>
									<td>Marks</td>
								</tr>
								{Object.keys(mark).length != 0 && (
									<>
										<tr>
											<td>Maths</td>
											<td>{mark.marks.subject_1}</td>
										</tr>
										<tr>
											<td>Physics</td>
											<td>{mark.marks.subject_2}</td>
										</tr>
										<tr>
											<td>Chemistry</td>
											<td>{mark.marks.subject_3}</td>
										</tr>
										<tr>
											<td>
												<b>Total Marks</b>
											</td>
											<td>
												<b>{mark.totalMarks}</b>
											</td>
										</tr>
									</>
								)}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</>
	);
}

MarksDetail.propTypes = {
	mark: PropTypes.object,
	marks: PropTypes.array.isRequired,
	loadMarks: PropTypes.func.isRequired,
};

function getMarkById(marks, id) {
	return marks.find((mark) => mark.student_id == id) || null;
}

function mapStateToProps(state, ownProps) {
	const studentId = ownProps.match.params.id;
	const mark = studentId && getMarkById(state.marks, studentId);
	return {
		mark,
		marks: state.marks,
	};
}
const mapDispatchToProps = {
	loadMarks,
};

export default connect(mapStateToProps, mapDispatchToProps)(MarksDetail);

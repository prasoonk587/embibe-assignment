import React from "react";
import { Route, Switch } from "react-router-dom";
import ScoreBoard from "./scoreBoard/ScoreBoard";
import MarksDetail from "./marksDetail/MarksDetail";
import Login from "./login/Login";

function App() {
	const userName = getCookie("userName");

	function getCookie(name) {
		var value = "; " + document.cookie;
		var parts = value.split("; " + name + "=");
		if (parts.length == 2) return parts.pop().split(";").shift();
	}

	return (
		<>
			{!userName && <Login />}
			{userName && (
				<>
					<div className="conatiner">
						<Switch>
							<Route exact path="/:id" component={MarksDetail} />
							<Route exact path="/" component={ScoreBoard} />
						</Switch>
					</div>
				</>
			)}
		</>
	);
}
export default App;

import React from "react";
import { connect } from "react-redux";
import { updateFilter } from "../../redux/actions/filterActions";
import PropTypes from "prop-types";
import logo from "../../assets/embibefullLogo.svg";

function HeaderFilter({ filter, updateFilter, showFilters }) {
	let timer;

	// Debounce the keystroke with 300ms delay
	function handleNameChange(event) {
		let text = event.target.value;
		clearTimeout(timer);
		timer = setTimeout(() => {
			updateFilter({ name: text });
		}, 300);
	}

	function handleSorting(event) {
		let sortBy = event.target.value;
		let order = 1;
		if (sortBy === filter.sortBy) {
			order = filter.order === 1 ? -1 : 1;
		}
		updateFilter({ ...filter, sortBy, order });
	}

	return (
		<nav className="navbar navbar-light bg-light">
			<div className="container-fluid">
				<img className="mr-4" src={logo} />
				{showFilters && (
					<form onSubmit={(e) => e.preventDefault()} className="form-inline">
						<input
							className="form-control mr-sm-2"
							placeholder="Search By Name"
							onChange={handleNameChange}
						/>
						<button
							className="btn btn-outline-success mr-sm-2"
							value="name"
							onClick={handleSorting}
						>
							Sort By Name
						</button>
						<button
							className="btn btn-outline-success my-2 my-sm-0"
							value="totalMarks"
							onClick={handleSorting}
						>
							Sort By Marks
						</button>
					</form>
				)}
			</div>
		</nav>
	);
}
HeaderFilter.propTypes = {
	filter: PropTypes.object,
	updateFilter: PropTypes.func.isRequired,
	showFilters: PropTypes.bool,
};

function mapStateProps(state) {
	return {
		filter: state.filter,
	};
}

const mapDispatchToProps = {
	updateFilter,
};

export default connect(mapStateProps, mapDispatchToProps)(HeaderFilter);

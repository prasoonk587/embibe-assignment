import React from "react";
import PropTypes from "prop-types";

function InputField({ setInputValue, name, value, placeholder, error, type }) {
	const handleChange = (e) => {
		const value = e.target.value;
		setInputValue({ value: value });
	};

	const handleBlur = (e) => {
		!e.target.value ? setInputValue({ error: "This field is required" }) : null;
	};

	return (
		<div>
			<input
				name={name}
				value={value}
				onChange={handleChange}
				onBlur={handleBlur}
				type={type}
				placeholder={placeholder}
			/>
			{error && <p>{error}</p>}
		</div>
	);
}

InputField.propTypes = {
	setInputValue: PropTypes.func,
	name: PropTypes.name,
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	placeholder: PropTypes.string,
	error: PropTypes.string,
	type: PropTypes.string,
};

export default InputField;

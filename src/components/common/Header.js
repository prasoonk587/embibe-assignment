import React from "react";
import PropTypes from "prop-types";
import HeaderFilter from "./HeaderFilter";

const Header = ({ showFilters }) => {
	return (
		<header>
			<HeaderFilter showFilters={showFilters} />
		</header>
	);
};

Header.propTypes = {
	showFilters: PropTypes.bool,
};

export default Header;

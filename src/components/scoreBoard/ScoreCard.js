import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import profileIcon from "../../assets/profileIcon.svg";

function ScoreCard({ mark }) {
	const { student_id, name, totalMarks, marks } = mark;
	return (
		<div className="col-sm-12 col-md-6 col-lg-4 marks-card">
			<div>
				<div className="first-row">
					<div className="img-wrap">
						<img src={profileIcon}></img>
					</div>
					<div className="name-wrap">
						<p className="name">
							<Link to={"/" + student_id}>{name}</Link>
						</p>
						<div>
							<span>Total Marks: </span>
							<span>{totalMarks}</span>
						</div>
					</div>
				</div>
				<div className="second-row">
					<div>
						<p>Maths</p>
						<p>{marks.subject_1}</p>
					</div>
					<div>
						<p>Physics</p>
						<p>{marks.subject_2}</p>
					</div>
					<div>
						<p>Chemistry</p>
						<p>{marks.subject_3}</p>
					</div>
				</div>
			</div>
		</div>
	);
}

ScoreCard.propTypes = {
	mark: PropTypes.object,
};

export default ScoreCard;

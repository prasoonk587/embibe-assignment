import React, { useEffect } from "react";
import { connect } from "react-redux";
import { loadMarks } from "../../redux/actions/marksActions";
import { compare } from "../../utils/compare";
import ScoreCard from "./ScoreCard";
import Header from "../common/Header";
import PropTypes from "prop-types";

function ScoreBoard({ marks, loadMarks, filter }) {
	// Filter student data by search input or sort
	const applyFilter = (marks) => {
		let filteredMarks = [...marks];

		// Filter if search input exist
		if ("name" in filter && filter.name) {
			filteredMarks = filteredMarks.filter(({ name }) =>
				name.startsWith(filter.name)
			);
		}

		// Sort if filter sort data exit
		if ("sortBy" in filter && filter.sortBy) {
			filteredMarks = filteredMarks.sort(compare(filter.sortBy, filter.order));
		}

		return filteredMarks;
	};

	useEffect(() => {
		if (marks.length === 0) {
			loadMarks().catch((err) => console.log(err));
		}
	}, []);

	return (
		<>
			<Header showFilters={true} />
			<div>
				<div className="col-md-12">
					<div className="score-board-wrap col-md-10 mx-auto">
						{applyFilter(marks).map((mark) => (
							<ScoreCard key={mark.student_id} mark={mark} />
						))}
					</div>
				</div>
			</div>
		</>
	);
}

ScoreBoard.propTypes = {
	marks: PropTypes.array,
	loadMarks: PropTypes.func,
	filter: PropTypes.object,
};

function mapStateProps(state) {
	return {
		marks: state.marks,
		filter: state.filter,
	};
}

const mapDispatchToProps = {
	loadMarks,
};

export default connect(mapStateProps, mapDispatchToProps)(ScoreBoard);

export function createFilterRedux(filter) {
	return { type: "FILTER", filter };
}

export function updateFilter(filter) {
	return function (dispatch) {
		dispatch(createFilterRedux(filter));
	};
}

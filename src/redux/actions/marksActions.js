import { getMarks } from "../../api/marksApi";

export function loadMarksRedux(marks) {
	return { type: "MARKS", marks };
}

function serializeMarks(marks) {
	let formattedMarks = Object.values(marks);
	formattedMarks = formattedMarks.map(({ marks, ...rest }) => {
		return {
			...rest,
			marks: {
				subject_1: Math.round(marks.subject_1 * 100) / 100,
				subject_2: Math.round(marks.subject_2 * 100) / 100,
				subject_3: Math.round(marks.subject_3 * 100) / 100,
			},
			totalMarks:
				Math.round(
					(marks.subject_1 + marks.subject_2 + marks.subject_3) * 100
				) / 100,
		};
	});
	return formattedMarks;
}

export function loadMarks() {
	return function (dispatch) {
		return getMarks()
			.then((marks) => {
				dispatch(loadMarksRedux(serializeMarks(marks)));
			})
			.catch((err) => {
				throw err;
			});
	};
}

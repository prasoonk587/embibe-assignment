export default function marksReducer(state = [], action) {
	switch (action.type) {
		case "MARKS":
			return [...state, ...action.marks];
		default:
			return state;
	}
}

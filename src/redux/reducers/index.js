import { combineReducers } from "redux";
import marks from "./marksReducer";
import filter from "./filterReducer";

const rootReducer = combineReducers({
	marks,
	filter,
});

export default rootReducer;

import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider as ReduxProivder } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import configureStore from "./redux/configureStore";
import App from "./components/App.js";
import "./style.scss";

const store = configureStore();

render(
	<ReduxProivder store={store}>
		<Router>
			<App />
		</Router>
	</ReduxProivder>,
	document.getElementById("app")
);

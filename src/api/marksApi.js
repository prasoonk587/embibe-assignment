// Get Marks Data From API
export function getMarks() {
	return fetch("https://api.npoint.io/1953ab244d9a35de08a6")
		.then((res) => {
			if (res.ok) return res.json();
		})
		.catch((err) => console.log("Marks API error - " + err));
}
